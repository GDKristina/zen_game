﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public GameObject CanvasToHide;
    private void Start()
    {
        Time.timeScale = 0f;
    }

    public void HideCanvas()
    {
        CanvasToHide.SetActive(false);
        Time.timeScale = 1f;
    }
}
